# Not So Basic : Devenv

Setting up an environment developpement can be percieved as a long boring and difficult operation.

Furthermore tools explanation, configuration is often overlooked in the formation programs.

Having the right tools help tremendously in the way of productivity.

Not So Basic : Devenv is a serie of 4 tutorials where we aim to have a nice basis for, productive and portable result that take the best of both worlds between linux and windows.

The parts are :    
- Linux on windows
- TBDA better shell with Oh-my-zsh
- TBDA A portable setup for web development
- TBDA IDEs and WSL 2 integration
- TBDA Docker as a devtool : local postgre container
