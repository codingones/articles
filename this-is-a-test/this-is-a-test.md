# This is a test

## Table of Contents

1. [Introduction](iIntroduction)
2. [Requirements](#requirements)
3. [Choices](#choices)
4. [Publication API](#publication-api)
5. [Next Step](#next-step)
6. [Conclusion](#conclusion)

## Introduction

I write this test article because I work on a fully automatized article publication workflow and I need an awesome service to (like [dev.to](dev.to)) to host my articles.  

I didn't plan to write a so long article, basically it's just a test...  
But it appears to become a real article about the publication workflow I want to set up, well, let's play the game to the end.  

## Requirements

Here are some constrains that drive the workflow I want to setup.  

  - Write articles without requiring a specific editor or an internet connection.
  - Work on an article with one or more co-writer.
  - Handle my articles through the source of truth of my choice.
  - Publish my articles on major publishing platforms, without having to connect to one of them.
  - Obviously overengineered, but do not have to host a custom tool to achieve this.
  - Do it for free

## Choices

### Git

As a happy Git user, this tool is my best choice to keep track of my articles versions and it allows me to work locally on my articles and share them with my team mates.  

### Markdown

I will go to Markdown as preferred text format because it is popular, simple, lightweight, and I am used to it :). Texts in Markdown can easily be write from any test editor and a plain tect format is easy to track with Git.  

### GitLab Ci

I don't know if I will change this later, but for the while I have good experiences with GitLab to host my Git repositories, but also with GitLab CI to run publication pipeline. So I plan to use it to trigger the publish process of my article when I want a new version to be online.  

### Docker

I don't know if I will require my source article to be processed before being published, if it is the case I will mount docker containers on the CI so that I can run pretty anything I want to during the publishing process.  

## Publication API

The final step of the whole publication process is actually publication on a publishing platforms, I am rather interested in the dev.to publication API which seems to have a lot of functionalities. So I plan to make publication calls from my CI to this API.  
Once published, I want to replicate this articles on at least another publishing platform, lets take Medium to begin using a canonical link to the original article hosted on dev.to  

## Next Step

Once all of this process up and running, I will try to replace dev.to with the blog section of my own website which actually do not have any blog section yet, but I want to avoid any dependency between being able to publish articles like this one and my personal website refactoring.  

## Conclusion

I really hope that my test will be conclusive because it would be a real shame to take so long to write a test article and see my test fail at the end...  

